@def title = "Expanding Man"


# Expanding Man

~~~
<center>
~~~
*that shape is my shade, there where I used to stand*
~~~
</center>
~~~

\begin{equation*}
\mathcal{Z}=
\int \mathcal{D}g~\mathcal{D}\psi~\mathcal{D}A~\mathcal{D}\Phi~\exp\left\{
    i\int d^{4}x\sqrt{-g}\left[
    \frac{m_{p}^{2}}{2}R 
    - \frac{1}{4}F^{a}_{\mu\nu}F^{\mu\nu}_{a}
    + i\bar{\psi}_{i}\cancel{\nabla}\psi_{i}
    + \left(\bar{\psi}_{i}^{L}V_{ij}\Phi\psi_{j}^{R} + \textrm{h.c.}\right)
    - |\nabla\Phi|^{2} - V(\Phi)
\right]\right\}
\end{equation*}

## Linux Configuration
- Description [here](https://expandingman.gitlab.io/dotfiles/)
- Repository: [`dotfiles`](https://gitlab.com/ExpandingMan/dotfiles)

## Julia Packages
- [LorentzGroup.jl](https://gitlab.com/ExpandingMan/LorentzGroup.jl)
- [Parquet2.jl](https://gitlab.com/ExpandingMan/Parquet2.jl)
- [Shapley.jl](https://gitlab.com/ExpandingMan/Shapley.jl)
- [QuickMenus.jl](https://gitlab.com/ExpandingMan/QuickMenus.jl)
- [Arxiv.jl](https://gitlab.com/ExpandingMan/Arxiv.jl)
- [Minio.jl](https://gitlab.com/ExpandingMan/Minio.jl)
- [I3.jl](https://gitlab.com/ExpandingMan/I3.jl)
- [Installer.jl](https://gitlab.com/ExpandingMan/Installer.jl)
- [ConsoleCodes.jl](https://gitlab.com/ExpandingMan/ConsoleCodes.jl)
- [NickCageErrors.jl](https://gitlab.com/ExpandingMan/nickcageerrors.jl)

## Other Projects
- [Writing CUDA data directly into the framebuffer with Vulkan in Julia](https://gitlab.com/ExpandingMan/VulkanShenanigans.jl)
- [Rendering some of Kip Thorne's wormholes](https://expandingman.gitlab.io/ThorneHole.jl/)
- [Comparing rust and zig for a command line utility](https://expandingman.gitlab.io/tvu-compare/)
- [Expanding Style](https://expandingman.gitlab.io/ExpandingStyle/) (Julia style guide)
- [A few Mandelbrot set related renderings](https://github.com/ExpandingMan/Mandelbrot.jl)
- [A MOS 6502 simulator in Julia](https://github.com/ExpandingMan/Sim6502.jl)
